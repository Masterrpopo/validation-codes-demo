package com.example.validationcodesdemo.controller;

import com.example.validationcodesdemo.model.ValidationCodeError;
import com.example.validationcodesdemo.model.ValidationErrorsWrapper;
import com.example.validationcodesdemo.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class DemoControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void containsUserFirstNameMissingErrorCode() throws Exception {
        final User user = new User();
        user.setFirstName(null);

        final String serializedUser = objectMapper.writeValueAsString(user);

        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON).content(serializedUser))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final String responseBody = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
        System.out.println("Response body: " + responseBody);

        final ValidationErrorsWrapper validationErrorsWrapper = objectMapper.readValue(responseBody, ValidationErrorsWrapper.class);

        final boolean containsUserFirstNameMissingErrorCode = validationErrorsWrapper.getValidationErrors().stream()
                .filter(ValidationCodeError.class::isInstance)
                .map(ValidationCodeError.class::cast)
                .anyMatch(validationCodeError -> Objects.equals(validationCodeError.getErrorCode(), "USER_FIRST_NAME_MISSING"));

        assertTrue(containsUserFirstNameMissingErrorCode);
    }

    @Test
    void containsUserFirstNameIncorrectFormatErrorCode() throws Exception {
        final User user = new User();
        user.setFirstName("asd");

        final String serializedUser = objectMapper.writeValueAsString(user);

        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON).content(serializedUser))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final String responseBody = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
        System.out.println("Response body: " + responseBody);

        final ValidationErrorsWrapper validationErrorsWrapper = objectMapper.readValue(responseBody, ValidationErrorsWrapper.class);

        final boolean containsUserFirstNameIncorrectFormatErrorCode = validationErrorsWrapper.getValidationErrors().stream()
                .filter(ValidationCodeError.class::isInstance)
                .map(ValidationCodeError.class::cast)
                .anyMatch(validationCodeError -> Objects.equals(validationCodeError.getErrorCode(), "USER_FIRST_NAME_INCORRECT_FORMAT"));

        assertTrue(containsUserFirstNameIncorrectFormatErrorCode);
    }
}
