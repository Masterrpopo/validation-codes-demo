package com.example.validationcodesdemo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ValidationCodeError extends ValidationError {
    private String errorCode;
}
