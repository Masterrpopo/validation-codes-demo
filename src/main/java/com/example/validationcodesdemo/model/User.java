package com.example.validationcodesdemo.model;

import com.example.validationcodesdemo.config.ErrorCodePayloads;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class User {
    private String username;
    private String password;
    @NotNull(message = "User's first name is missing", payload = ErrorCodePayloads.UserFirstNameMissing.class)
    @Pattern(regexp = "[A-Z][a-z]+", message = "User's first name must have correct format", payload = ErrorCodePayloads.UserFirstNameIncorrectFormat.class)
    private String firstName;
    private String lastName;
    private String email;
}
