package com.example.validationcodesdemo.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonTypeName("validationError")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "validationCodeError", value = ValidationCodeError.class)
})
public class ValidationError {
    protected String field;
    protected String message;
}
