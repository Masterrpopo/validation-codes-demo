package com.example.validationcodesdemo.errorhandling;

import com.example.validationcodesdemo.config.ErrorCodePayload;
import com.example.validationcodesdemo.model.ValidationErrorsWrapper;
import com.example.validationcodesdemo.model.ValidationCodeError;
import com.example.validationcodesdemo.model.ValidationError;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Payload;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Log4j2
@RestControllerAdvice
@RequiredArgsConstructor
public class ValidationControllerAdvice {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    ValidationErrorsWrapper methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException exception) {
        final List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        final List<ValidationError> validationErrors = fieldErrors.stream().map(this::buildValidationError).toList();

        return new ValidationErrorsWrapper(validationErrors);
    }

    private ValidationError buildValidationError(FieldError fieldError) {
        final String field = fieldError.getField();
        final String fieldErrorMessage = fieldError.getDefaultMessage();
        final Set<Class<? extends Payload>> payload = getPayload(fieldError);
        final Optional<ErrorCodePayload> errorCodePayload = findErrorCodePayload(payload);

        if (errorCodePayload.isPresent()) {
            final ValidationCodeError validationCodeError = new ValidationCodeError();
            validationCodeError.setField(field);
            validationCodeError.setMessage(fieldErrorMessage);
            validationCodeError.setErrorCode(errorCodePayload.get().getErrorCode());

            return validationCodeError;
        } else {
            final ValidationError validationError = new ValidationError();
            validationError.setField(field);
            validationError.setMessage(fieldErrorMessage);

            return validationError;
        }

    }

    private Optional<ErrorCodePayload> findErrorCodePayload(Set<Class<? extends Payload>> payload) {
        return payload.stream()
                .filter(ErrorCodePayload.class::isAssignableFrom)
                .map(payloadClass -> {
                    try {
                        return (ErrorCodePayload) payloadClass.getDeclaredConstructor().newInstance();
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        throw new IllegalStateException("Could not cast to ErrorCodePayload class", e);
                    }
                })
                .findFirst();
    }

    private Set<Class<? extends Payload>> getPayload(FieldError fieldError) {
        try {
            return fieldError.unwrap(ConstraintViolation.class).getConstraintDescriptor().getPayload();
        } catch (Exception e) {
            log.warn("Could not get payload from field error", e);
            return Collections.emptySet();
        }
    }

}
