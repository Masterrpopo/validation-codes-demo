package com.example.validationcodesdemo.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorCodePayloads {
    public static class UserFirstNameMissing implements ErrorCodePayload {
        @Override
        public String getErrorCode() {
            return "USER_FIRST_NAME_MISSING";
        }
    }

    public static class UserFirstNameIncorrectFormat implements ErrorCodePayload {
        @Override
        public String getErrorCode() {
            return "USER_FIRST_NAME_INCORRECT_FORMAT";
        }
    }

}
