package com.example.validationcodesdemo.config;

import jakarta.validation.Payload;

public interface ErrorCodePayload extends Payload {
    String getErrorCode();
}
