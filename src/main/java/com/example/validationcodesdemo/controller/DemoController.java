package com.example.validationcodesdemo.controller;

import com.example.validationcodesdemo.model.User;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@Log4j2
public class DemoController {
    @PostMapping("/user")
    @ResponseBody
    public String createUser(@Valid @RequestBody User user) {
        // mocked endpoint
        return "Created user successfully";
    }

}
